var AWS = require('aws-sdk');
var ses = new AWS.SES({ region: 'us-east-1' });

module.exports.send = async event => {
    var token = event.headers.Authorization;
    var sender_email = Buffer.from(token || 'none', 'base64').toString('ascii');

    var body = JSON.parse(event.body);
    var params = {
        Source: sender_email,
        Destination: {
            ToAddresses: [body.email],
            BccAddresses: [sender_email]
        },
        Message: {
            Subject: {
                Charset: 'UTF-8',
                Data: `We've Received Your Message!`
            },
            Body: {
                Html: {
                    Charset: 'UTF-8',
                    Data: body.body
                }
            }
        }
    }
    return ses.sendEmail(params).promise()
    .then(data => {
        return {
            statusCode: 200,
            body: JSON.stringify({
                message: 'Email Sent!'
            })
        }
    });
};
